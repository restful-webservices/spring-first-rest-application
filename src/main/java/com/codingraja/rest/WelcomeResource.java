package com.codingraja.rest;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class WelcomeResource {
	
	@GetMapping("/welcome")
	public Map<String, Object> welcome() {
		Map<String, Object> map = new HashMap<>();
		map.put("message", "Welcome to RESTFul Web Services with Spring MVC");
		return map;
	}

}
